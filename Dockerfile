FROM docker.digi-texx.vn/library/node:7.10.0-alpine
MAINTAINER X-Men x@digi-texx.vn
EXPOSE 3000
RUN npm config set registry "https://nexus.digi-texx.vn/repository/npm/"
# Create app directory
RUN mkdir -p /usr/src/app
# Create app logs folder
RUN mkdir -p /data/logs
WORKDIR /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app
# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install pm2 -g
RUN npm install
############################################################
# Change the docker default timezone from UTC to SGT
############################################################
#RUN apk update
#RUN apk add tzdata
#RUN cp /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
RUN echo "Asia/Ho_Chi_Minh" > /etc/timezone
RUN date
############################################################
CMD [ "npm", "start" ]
#CMD ["pm2-docker", "process.yml", "--only", "app"]