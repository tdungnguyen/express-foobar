const express = require('express');
const app = express();

app.get('/', function (req, res) {
    res.send('express-foobar is running');
});

var allowCrossDomain = function(req, res, next) {
    if ('OPTIONS' == req.method) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
      res.send(200);
    }
    else {
      next();
    }
};

app.use(allowCrossDomain);

app.get('/foo', function (req, res) {
    res.status(302);
    res.json(
        {
            "foo": "bar"
        }
    ).send();
});

app.get('/status', function (req, res) {
    res.status(200);
    res.json(
        {
            "status": "200"
        }
    );
    res.send();
});

app.listen(3000, function () {
    console.log('Foobar app is listening on port 3000!');
});